#!/usr/bin/env python
# -*- coding: utf-8 -*-

# TODO: Add check for missing configuration file

import argparse
import json
import logging
import subprocess
import sys
import time
import shlex
import os
import os.path
import socket
import curses
os.environ['COLUMNS'] = "120"

__author__  = "Roberto Gassira\'"
__email__   = "roberto.gassira@rsa.com"
__version__ = "1.0.1"

logFormat = '%(message)s'
logging.basicConfig(stream=sys.stdout, level=logging.INFO, format=logFormat)

NWMTLOGO="""
                                    _ _   _       _              _ 
 _ ____      __     _ __ ___  _   _| | |_(_)     | |_ ___   ___ | |
| '_ \ \ /\ / /____| '_ ` _ \| | | | | __| |_____| __/ _ \ / _ \| |
| | | \ V  V /_____| | | | | | |_| | | |_| |_____| || (_) | (_) | |
|_| |_|\_/\_/      |_| |_| |_|\__,_|_|\__|_|      \__\___/ \___/|_|
"""

class bcolors:
    BLUE = '\033[94m'
    BOLD = '\033[1m'
    RED = '\033[31m'
    GREEN = '\033[32m'
    ENDC = '\033[0m'
    CLRSCREEN = '\033c'

MONGO_EXPORT = "mongoexport --authenticationDatabase=admin --username=deploy_admin --type=json --quiet " \
               "--db=%s --collection=%s --password=%s --query '{%s}'"

CMD_SALT_PING = "salt '%s' test.ping"
CMD_SALT = "salt -L %s cmd.run '%s'"
CMD_SALT_FIND = "salt -L %s file.find '%s' type=f name=%s"
CMD_SALT_READ = "salt -L %s file.read %s"
CMD_SALT_COPY = "salt-cp %s %s %s"
HOSTS_FILE = "multi-hosts.json"

CURL_BASE = "curl --fail -m 5 -u %s:%s 'http://%s:%d%s'"
CURL_JSON = "?msg=ls&force-content-type=application/json&expiry=600"
#CAPTURE_RATE = "/decoder/stats/capture.rate?msg=get&force-content-type=text/plain&expiry=600"
STAT_URL = "?msg=get&force-content-type=text/plain&expiry=600"
LOG_DECODER_DEVICES_DISABLED = "/decoder/parsers/config/devices.disabled?msg=get&force-content-type=application/json&expiry=600"
LOG_DECODER_DEVICES_PATH = "/etc/netwitness/ng/envision/etc/devices"
CONFIG_URLS = {"Archiver" : ["/archiver/config","/archiver/collections*","*/database/config","*/index/config","*/sdk/config","/sdk/config","/archiver/devices*", "*/config"],
"LogDecoder": ["/decoder/config","/database/config", "/index/config", "/decoder/parsers/config"]}
REST_API_PORT = {"LogCollector": 50101,"LogDecoder": 50102, "Broker":50103, "Decoder":50104, "Concentrator":50105, "Archiver":50108}
VALID_SERVICE = ["AdminServer","Archiver","Malware","Warehouse","UEBA","EndpointLogHybrid","AnalystUI","ESAPrimary","ESASecondary","Concentrator","PacketHybrid","LogHybrid","LogDecoder","LogCollector","Decoder","IncLogDecoder","EndpointBroker","Broker"]
DANGEROUS_CMD = ["rm", "reboot", "halt", "shutdown", "rmdir", "systemctl", "kill"]
####### Log Parser Object Section ######
class LogParser:
    name = ""
    path = ""
    main_file = ""
    version = ""
    revision = ""
    custom_file = ""
    #main_hash = ""
    #custom_hash = ""

    def __init__(self, name,host):
        self.name = name
        self.path = LOG_DECODER_DEVICES_PATH + "/" + self.name
        parser_files = run_salt_find(host["uuid"], self.path, "\*"+self.name+"\*.xml")
        for filename in parser_files: 
            if filename.find("custom") != -1: self.custom_file = filename 
            else: self.main_file = filename
        
        for item in run_salt_cmd(host["uuid"], "head -10 " + self.main_file)[0]:
            if item.find("xml=") != -1 :  self.version=item.split("=")[1].strip('"')
            if item.find("revision=") != -1 :  self.revision=item.split("=")[1].strip('"')

        #self.main_hash = run_salt_cmd(host["uuid"], "sha1sum " + self.main_file)[0][0].strip(" ").split(" ")[0]
     
    def __str__(self):
        return 'Parser = {:16} - Ver = {:4} - Rev = {:4} - Main File = {:32} - Custom File = {:32}'.format(self.name, self.version, self.revision,self.main_file.split('/')[-1], self.custom_file.split('/')[-1])
    def __eq__(self, other):
        if self.name == other.name and self.path == other.path and self.main_file == other.main_file and self.custom_file == other.custom_file and self.version == other.version and self.revision == other.revision:
            return True
        else:
            return False
    def __hash__(self):
        return hash(self.name+self.path+self.main_file+self.custom_file+self.version+self.revision)


###################################################################################################
def print_info(msg):
    s1 = ""
    format = ';'.join([str(34)])
    s1 = '\x1b[%sm%s\x1b[0m' % (format, msg)
    print s1
    print 'RSA Netwitness NW-Multi-Tool - v. %s\n' %__version__

def get_host_info(host):
    return "Name = %-15s  IPv4 = %-15s  Version = %-10s  Available = %-7s Services = %-20s  UUID = %s  Serial = %s" % (
            host["displayName"], host["ipv4"], host["version"]["rawVersion"], host["is_available"], ','.join(host["installedServices"]), host["uuid"], host["serial"])

def get_current_time():
    return time.strftime("%Y/%m/%d - %H:%M:%S")

def error_msg(error):
    logging.error(bcolors.RED + error + bcolors.ENDC)

def error_exit(error):
    error_msg(error)
    sys.exit(1)

def run_cmd_complete(salt_cmd):
    run_env = os.environ.copy()
    run_env["OWB_ALLOW_NON_FIPS"] = "1"
    #print salt_cmd
    salt_cmd_run = subprocess.Popen(shlex.split(salt_cmd), stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=run_env)
    out, err = salt_cmd_run.communicate()
    return out, err

def run_cmd(cmd):
    run_env = os.environ.copy()
    run_env["OWB_ALLOW_NON_FIPS"] = "1"
    try:
        result = subprocess.check_output(shlex.split(cmd), env=run_env)
    except Exception as error_string:
        print('Unable to execute the command. Error: ' + str(error_string))
        return "-1"
    return result.strip()

def run_salt(uuid, salt_string):
    #print cmd
    result,error = run_cmd_complete(salt_string)
    lineresult = result.strip().split("\n")
    if len(lineresult) != 0 and lineresult[0].split(':')[0] != uuid:
        logging.error("UUID not valid!")
    return (lineresult[1:], error)

def run_salt_cmd(uuid, cmd):
    #print cmd
    result,error = run_cmd_complete(CMD_SALT %(uuid,cmd))
    lineresult = result.strip().split("\n")
    if len(lineresult) != 0 and lineresult[0].split(':')[0] != uuid:
        logging.error("UUID not valid!")
    return (lineresult[1:], error)

def run_salt_find(uuid,path,search):
    result,error = run_salt(uuid, CMD_SALT_FIND % (uuid, path, search))
    res = []
    for line in result: res.append(line.split(" ")[-1])
    return res

def run_salt_copy(uuid, sourcefile, destination):
    result,error = run_cmd_complete(CMD_SALT_COPY %(uuid,sourcefile,destination))
    lineresult = result.strip().split("\n")
    if len(lineresult) != 0 and lineresult[0].split(':')[0] != uuid:
        logging.error("UUID not valid!")
    return (lineresult[1:], error)

def read_info_file(dump_filename, is_json = True):
    try:
        with open(dump_filename, 'r') as infile:
            if is_json:
                import_info = json.load(infile)
            else:
                import_info = [line.rstrip('\n') for line in infile]
    except Exception as error:
        error_exit(error.message)
    return import_info

def run_mongo_export(db, collection, password, query, with_list=False):
    mongo_export_cmd = MONGO_EXPORT % (db, collection, password, query)
    if with_list:
        mongo_export_cmd += " --jsonArray "
    try:
        json_host_list = subprocess.check_output(shlex.split((mongo_export_cmd)))
    except Exception as error_string:
        error_exit("Impossible to export data from mongo! - Check deploy_admin password...")

    logging.debug('mongoexport of host collection in orchestration-server db = ' + json_host_list)
    return json.loads(json_host_list)
    # return json_host_list

def export_hosts_list(export_file, hosts_list):
    logging.info(bcolors.BLUE + "Exporting list of appliances in %s" % export_file + bcolors.ENDC)
    with open("%s/%s" % (os.path.dirname(os.path.realpath(__file__)), export_file), 'w') as exportfile:
        exportfile.write("Hostname;IP;Version;isAvailable;Services;UUID;Serial\n")
        for host in sorted(hosts_list, key=lambda i: socket.inet_aton(i['ipv4'])):
            exportfile.write("%s;%s;%s;%s;%s;%s;%s\n" % (host["displayName"], host["ipv4"], host["version"]["rawVersion"], host["is_available"],','.join(host["installedServices"]), host["uuid"], host["serial"]))

def get_host_final_list(filter,ip,readfile,verbose=True):
    hosts_info = read_info_file(HOSTS_FILE)
    host_final_list = []
    ip_list = []
    # Read ip from file
    if readfile:
        if verbose: logging.info(bcolors.BLUE + "Reading IP list from: %s " %readfile + bcolors.ENDC)
        ip_list = read_info_file(readfile,False)
    # Select hosts
    for host in hosts_info["hosts_list"]:
        if host["is_available"] == True:
            if readfile and host["ipv4"] in ip_list:
                host_final_list.append(host)
                if verbose: logging.info("Read IP: %-15s - Hostname: %s" % (host["displayName"], host["ipv4"]))
            elif ip and host["ipv4"] == ip:
                host_final_list.append(host)
                if verbose: logging.info(bcolors.BLUE + "Running on IP %s - Hostname %s" % (host_final_list[0]["displayName"], host_final_list[0]["ipv4"]) + bcolors.ENDC)
                break
            elif not ip and not readfile and filter == "all":
                host_final_list.append(host)
            elif not ip and not readfile and filter == "noas":
                if host["installedServices"] != ["AdminServer"]:
                    host_final_list.append(host)
            elif not ip and not readfile and filter in host["installedServices"]:
                host_final_list.append(host)

    if len(host_final_list) == 0:
        if readfile:
            error_exit("Incorrect IP Addresses in the file: %s!" % readfile)
        elif ip:
            error_exit("Incorrect IP Address: %s!" % ip)
        else:
            error_exit("Incorrect filter parameter: %s!" %filter)

    return host_final_list

def convert_result_into_config_dict(result, config):
    if not result:
        return config
    result = json.loads(result)
    for info in result["nodes"]:
        config[info["path"]] = {"name": info["name"], "display": info["display"]}
        if "value" in info:
            config[info["path"]]["value"] = info["value"]
    return config

def compare_configs(first,second):
    first_set = set(first.keys())
    second_set = set(second.keys())

    # Calculating differences
    same_conf = sorted((first_set & second_set))
    first_conf = sorted((first_set - second_set))
    second_conf = sorted((second_set - first_set))
    if len(first_conf) > 0:
        logging.info(bcolors.RED + "Configurations found only on master host:" + bcolors.ENDC)
        for conf in first_conf:
            logging.info(bcolors.BLUE + "%s" % first[conf] + bcolors.ENDC)

    if len(second_conf) > 0:
        logging.info(bcolors.RED + "Configurations found only on compared host:" + bcolors.ENDC)
        for conf in second_conf:
            logging.info(bcolors.GREEN + "%s" % second[conf] + bcolors.ENDC)

    logging.info(bcolors.BLUE + "\n[%s] Comparing parameters of matching configurations:" % get_current_time() + bcolors.ENDC)
    # Check the same keys
    for check_key in same_conf:
        if first[check_key] != second[check_key]:
            logging.info(bcolors.RED + "[%s]: " %check_key +  bcolors.ENDC)
            logging.info(bcolors.BLUE +  "Master   => %s " % first[check_key] + bcolors.ENDC)
            logging.info(bcolors.GREEN + "Compared => %s\n " % second[check_key] + bcolors.ENDC)

def compare_sets(first_set,second_set,type_to_compare):
    # Calculating differences
    same_set = sorted((first_set & second_set))
    first_set_only = sorted((first_set - second_set))
    second_set_only = sorted((second_set - first_set))
    if len(first_set_only) > 0:
        logging.info(bcolors.RED + "%s found only on master host:" %type_to_compare + bcolors.ENDC)
        for item in first_set_only:
            logging.info(bcolors.BLUE + "%s" % item + bcolors.ENDC)

    if len(second_set_only) > 0:
        logging.info(bcolors.RED + "%s found only on compared host:" %type_to_compare + bcolors.ENDC)
        for item in second_set_only:
            logging.info(bcolors.BLUE + "%s" % item + bcolors.ENDC)

    # Show the same keys
    #logging.info(bcolors.GREEN + "Same %s:" % (type_to_compare) + bcolors.ENDC)
    #for item in same_set:
    #    logging.info(bcolors.BLUE +  "%s" % item + bcolors.ENDC)
        
###################################################################################################
##########################                 Methods                     ############################
###################################################################################################
def host_list(export_file=None):
    hosts_info = read_info_file(HOSTS_FILE)
    logging.info("Server Info:\nHostname = %s - IPv4 = %s\n" % (
        hosts_info["server_info"]["hostname"], hosts_info["server_info"]["ipv4"]))
    logging.info("Hosts info:")
    for host in sorted(hosts_info["hosts_list"], key=lambda i: socket.inet_aton(i['ipv4'])):
        logging.info(get_host_info(host))
    if export_file:
        export_hosts_list(export_file,hosts_info["hosts_list"])


def run_multi_cmd(cmd,force,filter,ip,readfile):
    # Check if comand is dangerous
    for cmd_dangerous in DANGEROUS_CMD:
        if cmd.find(cmd_dangerous) != -1 and force is False:
            error_exit("Dangerous command detected. To run use the parameter --force")
    host_final_list = get_host_final_list(filter,ip,readfile)
    if force:
        logging.info(bcolors.RED + "[%s] Running command in FORCE mode: %s" % (get_current_time(), cmd) + bcolors.ENDC)    
    else:
        logging.info(bcolors.BLUE + "[%s] Running command: %s" % (get_current_time(), cmd) + bcolors.ENDC)
    # Run command on hosts
    for host in sorted(host_final_list, key=lambda i: socket.inet_aton(i['ipv4'])):
        result,error = run_salt_cmd(host["uuid"], cmd)
        if error:
            logging.info(bcolors.RED+"%-20s[%s]:" %(host["displayName"], host["ipv4"])+ bcolors.ENDC)
        else:
            logging.info(bcolors.GREEN + "[%s]:%s" % (host["displayName"], host["ipv4"]) + bcolors.ENDC)
            for line in result:
                logging.info(line)

def run_multi_copy(sourcefile, destination,filter,ip,readfile,isbigfile):
    result = run_cmd("sha256sum %s" %sourcefile)
    if result.split(" ")[2] != sourcefile:
        error_exit("Multi copy error: error in sha256sum calculation.")
    filesha256 = result.split(" ")[0]

    host_final_list = get_host_final_list(filter,ip,readfile)
    logging.info(bcolors.BLUE + "[%s] Copying file: %s" % (get_current_time(), sourcefile) + bcolors.ENDC)

    # Copy file on hosts
    for host in sorted(host_final_list, key=lambda i: socket.inet_aton(i['ipv4'])):
        if isbigfile:
            result = run_cmd("scp %s %s:%s" % (sourcefile,host["ipv4"],destination))
            print result
        else:
            result,error = run_salt_copy(host["uuid"], sourcefile, destination)
            if error:
                logging.info(bcolors.RED + "Error on %s[%s]: %s" % (host["displayName"], host["ipv4"],error) + bcolors.ENDC)
                continue
            else:
                logging.info(bcolors.GREEN + "%s[%s]:" % (host["displayName"], host["ipv4"]) + bcolors.ENDC)
                for line in result:
                    logging.info(line)
        # Check shasum
        result, error = run_salt_cmd(host["uuid"], "sha256sum %s%s" %(destination,sourcefile.split('/')[-1]))
        if error:
            logging.info(bcolors.RED + "File command error: %s" %error + bcolors.ENDC)
        else:
            if result[0].strip().split(" ")[0] == filesha256:
                logging.info(bcolors.GREEN + "File checksum OK." + bcolors.ENDC)
            else:
                logging.info(bcolors.RED + "File checksum INVALID." + bcolors.ENDC)


def service_stat(stat,service,username,password,filter,ip,readfile):
    host_final_list = get_host_final_list(filter, ip, readfile)
    # Check if selected hybrid without service
    for host in host_final_list:
        if any("Hybrid" in installedService for installedService in host["installedServices"]) and service is None:
            error_exit("Please select specific service for Hybrid hosts with -sr parameter ( es. -sr LogDecoder).")
    
    time.sleep(1)
    # Init screen
    scr = curses.initscr()
    curses.curs_set(False)
    scr.clear()
    scr.refresh()
    scr.addstr(0, 0, "Stat: %s" %stat)
    exitOnError = None
    while exitOnError is None:
        counter = 2
        try:
            for host in sorted(host_final_list, key=lambda i: socket.inet_aton(i['ipv4'])):
                result,error = run_cmd_complete(CURL_BASE %(username, password, host["ipv4"], REST_API_PORT[service if service is not None else host["installedServices"][0]],stat+STAT_URL))
                if error.find("error") != -1 or len(result) == 0:
                    exitOnError = error
                    break
                scr.addstr(counter,0,"%-15s:                     ")
                scr.addstr(counter,0,"%-15s: %s" %(host["ipv4"],result.split()[0]))
                counter = counter + 1
            scr.refresh()
            time.sleep(1)
        except KeyboardInterrupt:
            break    
    
    # Exit code
    curses.curs_set(True)
    curses.endwin()
    if exitOnError is not None and "404" in exitOnError:
        logging.info("Error: %s not correct!" %stat)
    elif exitOnError:
        logging.info(exitOnError.split("curl")[1])
    else: 
        logging.info("Stats closes. Bye!")
    sys.exit()

def get_master_config(master_host,service, username, password):
    master_config = {}
    star_url = []
    for config_url in CONFIG_URLS[service]:
        if len(star_url):
            if config_url.startswith("*"):
                for star in star_url: 
                    new_config_url = star+config_url[1:]
                    result, error = run_cmd_complete(CURL_BASE % (username,password,master_host["ipv4"], REST_API_PORT[service], new_config_url+CURL_JSON))
                    master_config = convert_result_into_config_dict(result, master_config)
                continue
            elif not config_url.startswith("*"):
                star_url = []

        if config_url.endswith("*"):
            result, error = run_cmd_complete(CURL_BASE % (username,password, master_host["ipv4"], REST_API_PORT[service], config_url[:-1]+CURL_JSON))
            result = json.loads(result)
            for info in result["nodes"]:
                star_url.append(info["path"])
        else:
            result, error = run_cmd_complete(CURL_BASE % (username,password, master_host["ipv4"], REST_API_PORT[service], config_url+CURL_JSON))
            master_config = convert_result_into_config_dict(result, master_config)
    return master_config   
    

def host_conf_ng(master, compare, service,username,password):
    logging.info(bcolors.BLUE + "[%s] Starting configuration match with: " % get_current_time() + bcolors.ENDC)
    master_host = get_host_final_list(None, master, None, False)[0]
    logging.info(get_host_info(master_host))
    
    compare_host = get_host_final_list(None, compare, None, False)[0]
    logging.info(bcolors.BLUE + "[%s] Host to compare: " % get_current_time() + bcolors.ENDC)
    logging.info(get_host_info(compare_host))

    logging.info(bcolors.BLUE + "[%s] Getting configs from master." % get_current_time() + bcolors.ENDC)
    master_config = get_master_config(master_host, service, username,password)
    logging.info(bcolors.BLUE + "[%s] Getting configs from host to compare." % get_current_time() + bcolors.ENDC)
    compare_config = get_master_config(compare_host, service, username,password)
    
    # Compare configurations
    compare_configs(master_config, compare_config)

    if service == "LogDecoder":
        logging.info(bcolors.BLUE + "[%s] Comparing Log Parsers: " % get_current_time() + bcolors.ENDC)
        master_log_parsers = logdecoder_conf(master_host, username,password)
        compare_log_parsers = logdecoder_conf(compare_host, username,password)
        compare_sets(master_log_parsers, compare_log_parsers, "Log Parser")


def logdecoder_conf(host,username,password):
    logging.info(bcolors.BLUE + "Acquiring Log Parser List and Info from %s" %host["ipv4"] + bcolors.ENDC)
    result = json.loads(run_cmd_complete(CURL_BASE %(username, password, host["ipv4"], REST_API_PORT["LogDecoder"],LOG_DECODER_DEVICES_DISABLED))[0])
    dev_disabled = set(result["string"].split(","))
    dev_all = set(run_cmd_complete(CMD_SALT % (host["uuid"], "ls %s" % LOG_DECODER_DEVICES_PATH))[0].replace(" ","").split("\n")[1:-1])
    dev_active = sorted(dev_all - dev_disabled)
    logging.info(bcolors.BLUE + "[%s] Found %d log parsers enabled. Starting log parser analysis: " % (get_current_time(), len(dev_active)) + bcolors.ENDC)
    #logging.info("Device Enabled = " + (', ').join(dev_active))
    log_parsers = set()
    for dev in dev_active:
        log_parser = LogParser(dev,host)
        log_parsers.add(log_parser)
        sys.stdout.write('.')
        sys.stdout.flush()
    logging.info(bcolors.BLUE + "\n[%s] Analysis completed." % get_current_time() + bcolors.ENDC)
    return log_parsers

def init_data(password):
    hosts_info = {}
    n_services = {}
    # Get Server Info
    server_query = '"installedServices":["AdminServer"]'
    server_info = run_mongo_export("orchestration-server", "host", password, server_query)
    # PATCH for NW 11.2.x
    if not "ipv4" in server_info.keys():
        server_info["ipv4"] = server_info["hostname"]
    hosts_info["server_info"] = server_info

    # Get host info
    hosts_info["hosts_list"] = []
    # Get Host List Dump
    logging.info(bcolors.BLUE + "Init list of appliances..." + bcolors.ENDC)
    hosts_info_raw = run_mongo_export("orchestration-server", "host", password, "", True)
    for host in hosts_info_raw:
        if len(host["installedServices"]) == 0:
            continue
        host["uuid"] = host["_id"]
        # Remove useless keys
        del host["thirdParty"], host["meta"], host["_class"], host["_id"]
        # Patch for nw 11.2.x
        if "ipv4" not in host.keys():
            host["ipv4"] = host["hostname"]
        hosts_info["hosts_list"].append(host)
        # Get number of services
        for service in host["installedServices"]:
            if not service in n_services.keys():
                n_services[service]=1
            else:
                n_services[service] += 1

    logging.info(bcolors.BLUE + "Appliance discovered: Total = %d " %len(n_services) + bcolors.ENDC)
    for service in sorted(n_services.keys()):
        logging.info("%-25s = %-15s" % (service, n_services[service]))

    # Checking if node is up
    logging.info(bcolors.BLUE + "Checking appliance status:" + bcolors.ENDC)
    for host in sorted(hosts_info["hosts_list"], key=lambda i: socket.inet_aton(i['ipv4'])):
        host_status = run_cmd_complete(CMD_SALT_PING % host["uuid"])[0]
        if host_status.strip().find("True") != -1:
            host["is_available"] = True
            logging.info("%-15s: " %host["ipv4"] + bcolors.GREEN + "UP" + bcolors.ENDC)
            serial_number = run_salt_cmd(host["uuid"],"dmidecode -s system-serial-number")[0][0].replace(" ","").strip()
            host["serial"] = serial_number
        else:
            host["is_available"] = False
            host["serial"] = "--"
            logging.info("%-15s: " %host["ipv4"] + bcolors.RED + "DOWN" + bcolors.ENDC)

    logging.info(bcolors.BLUE + "Saving list of appliances in %s" %HOSTS_FILE + bcolors.ENDC)
    with open("%s/%s" % (os.path.dirname(os.path.realpath(__file__)), HOSTS_FILE), 'w') as outfile:
        json.dump(hosts_info, outfile)
        outfile.write("\n")

    logging.info(bcolors.BLUE + "Appliance List:" + bcolors.ENDC)
    host_list()

if __name__ == '__main__':
    print_info(NWMTLOGO)
    parser = argparse.ArgumentParser()
    parser_commands = parser.add_subparsers(title='Type of operation', dest="operation")
    # Init 
    parser_init = parser_commands.add_parser("init", help="Init file with hosts information")
    parser_init.add_argument('-d', '--deploy_password', default='', help='The deploy_admin user password (required)',required=True)
    parser_init.epilog = "example: "+parser_init.prog + " -d netwitness"
    # List
    parser_list = parser_commands.add_parser("list", help="Show appliance list")
    parser_list.add_argument('-e', '--export', default=None, help='Export list in csv file')
    parser_list.epilog = "example: "+parser_list.prog + " -e export.csv"
    # Group arguments declaration
    parent_parser = argparse.ArgumentParser(add_help=False)
    cmd_group = parent_parser.add_mutually_exclusive_group()
    cmd_group.add_argument('-f', '--filter', default="all",
                           help="all for all (default) or noas for all except the AdminServer or filter on the type of appliance or (%s)" % VALID_SERVICE)
    cmd_group.add_argument('-i', '--ip', help="Run command on appliance with ip address")
    cmd_group.add_argument('-l', '--listfile', help="Run command on appliance listed in readfile")

    # Multi option arguments
    # cmd
    parser_cmd = parser_commands.add_parser("cmd", help="Command to run on multiple hosts", parents=[parent_parser])
    parser_cmd.add_argument('-c', '--command', required=True, help="Command to run")
    parser_cmd.add_argument('--force', default=False, help="Force dangerous command execution", action='store_true')
    parser_cmd.epilog = "example: " + parser_cmd.prog + " -f noas -c date"

    # multicopy
    parser_multicopy = parser_commands.add_parser("mcopy", help="Copy files on several appliances", parents=[parent_parser])
    parser_multicopy.add_argument("-sf", '--sourcefile', required=True, help="File to copy")
    parser_multicopy.add_argument("-df", '--destination', default="/root", help="Destination folder")
    parser_multicopy.add_argument('--bigfile', default=False, help='Copy done via SCP if file to copy is bigger then 100K', action='store_true')
    parser_multicopy.epilog = "example: " + parser_multicopy.prog + " -f LogHybrid -sf /etc/snmp/snmpd.conf -df /etc/snmp/"
    # stat
    parser_stat = parser_commands.add_parser("stat", help="Get service stats", parents=[parent_parser])
    parser_stat.add_argument('-s', '--stat', default="/decoder/stats/capture.rate", help="Stat to read (default=/decoder/stats/capture.rate)")
    parser_stat.add_argument('-sr', '--service', required=False, help="Service Type")
    parser_stat.add_argument('-u', '--username', required=False, help="REST API username (default = admin)", default='admin')
    parser_stat.add_argument('-p', '--password', required=False, help="REST API password (default = netwitness)", default='netwitness')
    parser_stat.epilog = "example: " + parser_stat.prog + " -f LogHybrid  -sr LogDecoder -s /decoder/stats/capture.packet.rate"
    # conf
    parser_conf = parser_commands.add_parser("conf", help="[BETA] Get and compare host configurations(LogDecoder,Archiver)")
    parser_conf.add_argument('-m', '--master', required=True, help="IP of appliance to match the configution ")
    parser_conf.add_argument('-c', '--compare', required=True, help="IP of appliance to compare the configution ")
    parser_conf.add_argument('-s', '--service', required=True, help="Service configution to compare: %s" %REST_API_PORT.keys())
    parser_conf.add_argument('-u', '--username', required=False, help="REST API username (default = admin)", default='admin')
    parser_conf.add_argument('-p', '--password', required=False, help="REST API password (default = netwitness)", default='netwitness')
    parser_conf.epilog = "example: " + parser_conf.prog + " -m 172.16.1.164 -c 172.16.1.165 -s LogDecoder"
    parsed_arguments = parser.parse_args()
    if parsed_arguments.operation == "init":
        init_data(parsed_arguments.deploy_password)
        sys.exit(0)

    # Check to run after init
    if not os.path.exists(HOSTS_FILE) and parsed_arguments.operation: error_exit(parser.prog + " not initialized\nPlease run the init command before!")

    if parsed_arguments.operation == "list":
        host_list(parsed_arguments.export)
    elif parsed_arguments.operation == "cmd":
        run_multi_cmd(parsed_arguments.command, parsed_arguments.force,  parsed_arguments.filter, parsed_arguments.ip, parsed_arguments.listfile)
    elif parsed_arguments.operation == "mcopy":
        run_multi_copy(parsed_arguments.sourcefile,parsed_arguments.destination, parsed_arguments.filter, parsed_arguments.ip, parsed_arguments.listfile,parsed_arguments.bigfile)
    elif parsed_arguments.operation == "stat":
        service_stat(parsed_arguments.stat, parsed_arguments.service,parsed_arguments.username, parsed_arguments.password, parsed_arguments.filter, parsed_arguments.ip, parsed_arguments.listfile)
    elif parsed_arguments.operation == "conf":
        host_conf_ng(parsed_arguments.master, parsed_arguments.compare, parsed_arguments.service,parsed_arguments.username, parsed_arguments.password)
    else:
        parser.print_help()
