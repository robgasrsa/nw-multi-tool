<pre>
                                    _ _   _       _              _ 
 _ ____      __     _ __ ___  _   _| | |_(_)     | |_ ___   ___ | |
| '_ \ \ /\ / /____| '_ ` _ \| | | | | __| |_____| __/ _ \ / _ \| |
| | | \ V  V /_____| | | | | | |_| | | |_| |_____| || (_) | (_) | |
|_| |_|\_/\_/      |_| |_| |_|\__,_|_|\__|_|      \__\___/ \___/|_|
</pre>
# nw-multi-tool
Tool for helping in RSA Netwitness platform administration allowing commands execution on multiple hosts, services configuration comparision, cumulative service statistics and files pushing from NW Server to the other platform's hosts.
